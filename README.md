#Projecte web

**Integrants:** Joel Duran i Alex Valero.

**Tema del web:** Viatjes.

**Títol del web:** ITravelerB

**Esquema del lloc web:**

- index:

     - És la pagina principal.
     - Situa tots els apartats de interès (Historia, Cultura, etc.) amb els països que recomanem i una breu descripció.

- nosaltres:

     - Conté una presentació de nosaltres i l'objectiu de la pagina.
     - Conté informació de contacte.
     - Conté un formulari per fer-nos arribar qualsevol dubte.  

- pagines del països (hi han 30 pagines diferents): 

    - Contenen un Slider amb tres imatges diferents i una breu explicació.